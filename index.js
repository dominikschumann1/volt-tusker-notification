const webpush = require('web-push');
const express = require('express');
const path = require('path');
const Queue = require('bull');
const request = require('request');
const { error } = require('console');
const fs = require('fs');
require('dotenv').config({ path: 'variables.env' });

module.exports.publicVapidKey = process.env.PUBLIC_VAPID_KEY;
module.exports.privateVapidKey = process.env.PRIVATE_VAPID_KEY;

// Replace with your email
webpush.setVapidDetails('mailto:ds@sillyscientists.de', this.publicVapidKey, this.privateVapidKey);

const app = express();

app.use(require('body-parser').json());

app.use(express.static(path.join(__dirname, 'client')));

app.get('/notifications', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/notifications/client.js', (req, res) => {
  res.sendFile(path.join(__dirname, 'client.js'));
});

app.get('/notifications/test.json', (req, res) => {
  res.send(JSON.parse(fs.readFileSync(path.join(__dirname, 'test.json'))));
})

app.get('/notifications/worker.js', (req, res) => {
  res.sendFile(path.join(__dirname, 'worker.js'));
});

const subscribers = [];

app.post('/notifications/subscribe', (req, res) => {
  const subscription = req.body;
  if (subscribers.every((value) => subscription.endpoint != value.endpoint)) subscribers.push(subscription);
  console.log(subscribers.length);

  console.log(subscription);

  // webpush.sendNotification(subscription, payload).catch(error => {
  //   console.error(error.stack);
  // });
});

app.post('/notifications/message', (req, res) => {
  const key = req.body.secret;
})

app.listen(process.env.PORT, () => {
  console.log("Started Server on Port 6100");
})

function pushMessage(text) {
  console.log("send");
  const payload = JSON.stringify({ title: 'Volt Online Parteitag', body: text });
  subscribers.forEach((sub) => {
    webpush.sendNotification(sub, payload).catch(error => {
      console.error(error.stack);
    });
  })
}

let updater = new Queue('push Queue');

let messages = [];
setInterval(() => {
  request('https://voltcharts.sillyscientists.de/data', (error, response, body) => {
    if (!error && response.statusCode == 200) {
      const res = JSON.parse(body);
      if (messages.length != res.results.messages.length) {
        pushMessage(res.results.messages[0].body);
        messages = res.results.messages;
      }
    } else {
      console.error(error);
    }
  })
}, 500);