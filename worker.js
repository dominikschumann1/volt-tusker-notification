console.log('Loaded service worker!');

self.addEventListener('push', ev => {
  const data = ev.data.json();
  console.log('Got push', data);
  self.registration.showNotification(data.title, {
    body: data.body,
    icon: 'https://portal.voltdeutschland.org/favicon.ico',
    badge: 'https://portal.voltdeutschland.org/favicon.ico',
    vibrate: [500,110,500,110,450,110,200,110,170,40,450,110,200,110,170,40,500]
  });
});

self.addEventListener('notificationclick', function(event) {
    const clickedNotification = event.notification;
    clickedNotification.close();
  
    // Do something as the result of the notification click
    event.waitUntil(
        clients.openWindow('https://tusker.volt.codes/live_event/11')
      );
  });