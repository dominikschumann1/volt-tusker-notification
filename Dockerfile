FROM node:12.18.4-alpine

WORKDIR /usr/share/speaker-web

COPY package*.json ./

RUN npm i

COPY . .

EXPOSE 6100

CMD ["npm", "start"]